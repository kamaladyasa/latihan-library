from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
import base64
import uuid
from flask_bcrypt import Bcrypt, check_password_hash
import re

app = Flask(__name__)
db = SQLAlchemy()
bcrypt = Bcrypt(app)

app.config['SECRET_KEY']='secret'
app.config['SQLALCHEMY_DATABASE_URI']='postgresql://postgres:P@ssw0rd@localhost:5432/library'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)

class Usert(db.Model):
    user_id = db.Column(db.Integer, primary_key=True, index=True)
    name = db.Column(db.String(40), nullable=False)
    password = db.Column(db.String, nullable=False)
    email = db.Column(db.String(28), nullable=False, unique=True)
    borrower_user = db.relationship('Rent', backref = 'buser', lazy = 'dynamic')

    def __repr__(self):
        return f'Usert <{self.email}>'

class Book(db.Model):
    code_book = db.Column(db.Integer, primary_key=True, index = True)
    title = db.Column(db.String(100), nullable=False)
    language = db.Column(db.String(20), nullable=False)
    year = db.Column(db.Integer)
    total_book = db.Column(db.Integer, nullable=False, default = 1)
    borrower_book = db.relationship('Rent', backref = 'bbook', lazy = 'dynamic')

    def __repr__(self):
        return f'Book <{self.title}>'

class Rent(db.Model):
    rent_id = db.Column(db.Integer, primary_key=True, index = True)
    rent_from_date = db.Column(db.String)
    rent_to_date = db.Column(db.String)
    actual_return_date = db.Column(db.String)
    # status rent of book, if true means book have returned otherwise book have not returned yet
    status = db.Column(db.Boolean, default = False)
    user_id = db.Column(db.Integer, db.ForeignKey('usert.user_id'), nullable = False)
    code_book = db.Column(db.Integer, db.ForeignKey('book.code_book'), nullable = False)

    def __repr__(self):
        return f'Rent <{self.rent_id}>'

def login():
    res = request.headers.get("Authorization")
    a = res.split()
    u = base64.b64decode(a[-1]).decode('utf-8')
    b = u.split(":")
    return b

def encrypt(password):
    return bcrypt.generate_password_hash(password).decode('utf-8')

@app.route('/users/')
def get_users():
    return jsonify([
        {
            'user_id': user.user_id, 'name': user.name, 'password': user.password,
            'email': user.email
        } for user in Usert.query.all()
    ])

@app.route('/users/<id>/')
def get_user(id):
    print(id)
    user = Usert.query.filter_by(user_id=id).first_or_404()
    return {
        'user_id': user.user_id, 'name': user.name,
        'password': user.password, 'email': user.email
    }

@app.route('/users/', methods=['POST'])
def create_user():
    data = request.get_json()
    if not 'name' in data or not 'email' or not 'password' in data:
        return jsonify({
            'error': 'Bad Request',
            'message': 'Name or email or password not given'
        }), 400
    if len(data['name']) < 4:
        return jsonify({
            'error': 'Bad Request',
            'message': 'Name must be contain minimum of 4 letters'
        }), 400
    pattern = '^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$'
    if not (re.search(pattern, data['email'])):
        return jsonify({
            'error': 'Bad Request',
            'message': 'Not a valid email'
        }), 400
    count=0
    while True:
        if len(data['password']) < 6:
            count = -1
            break
        elif not re.search("[a-z]", data['password']):
            count = -1
            break
        elif not re.search("[0-9]", data['password']):
            count = -1
            break
        elif re.search("\s", data['password']):
            count = -1
            break
        else:
            count = 0
            break
    if count == -1:
        return jsonify({
            'error': 'Bad Request',
            'message': 'Password not valid must be contain alphabet and number minimum of 6 letters'
        }), 400
    pw_hash = encrypt(data['password'])
    u = Usert(
        name = data['name'],
        email = data['email'],
        password = pw_hash
    )
    db.session.add(u)
    db.session.commit()
    return {
        'user_id': u.user_id, 'name': u.name,
        'password': u.password, 'email': u.email
    }, 201

@app.route('/users/<id>/', methods=['PUT'])
def update_user(id):
    data = request.get_json()
    user = Usert.query.filter_by(user_id=id).first_or_404()
    if (not 'name' in data) and (not 'email' in data) and (not 'password' in data):
        return {
            'error': 'Bad Request',
            'message': 'Name or email or password field needs to be present'
        }, 400   
    if 'name' in data:
        if len(data['name']) < 4:
            return jsonify({
                'error': 'Bad Request',
                'message': 'Name must be contain minimum of 4 letters'
            }), 400
        user.name = data['name']
    if 'email' in data:
        pattern = '^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$'
        if not (re.search(pattern, data['email'])):
            return jsonify({
                'error': 'Bad Request',
                'message': 'Not a valid email'
            }), 400
        user.email = data['email']
    if 'password' in data:
        count=0
        while True:
            if len(data['password']) < 6:
                count = -1
                break
            elif not re.search("[a-z]", data['password']):
                count = -1
                break
            elif not re.search("[0-9]", data['password']):
                count = -1
                break
            elif re.search("\s", data['password']):
                count = -1
                break
            else:
                count = 0
                break
        if count == -1:
            return jsonify({
                'error': 'Bad Request',
                'message': 'Password not valid must be contain alphabet and number minimum of 6 letters'
            }), 400
        pw_hash = encrypt(data['password'])
        user.password = pw_hash
    db.session.commit()
    return jsonify({
        'user_id': user.user_id,
        'name': user.name, 'password': user.password,
        'email': user.email
    })

@app.route('/users/<id>/', methods=['DELETE'])
def delete_user(id):
    user = Usert.query.filter_by(user_id=id).first_or_404()
    db.session.delete(user)
    db.session.commit()
    return {
        'success': 'Data deleted successfully'
    }

@app.route('/books/')
def get_books():
    return jsonify([
        {
            'code_book': book.code_book, 'title': book.title, 'language': book.language,
            'year': book.year, 'total_book': book.total_book
        } for book in Book.query.all()
    ])

@app.route('/books/<id>/')
def get_book(id):
    print(id)
    book = Book.query.filter_by(code_book=id).first_or_404()
    return {
        'code_book': book.code_book, 'title': book.title, 'language': book.language,
        'year': book.year, 'total_book': book.total_book
    }

@app.route('/books/', methods=['POST'])
def create_book():
    data = request.get_json()
    if not 'code_book' or not 'title' or not 'language' or not 'year' or not 'total_book' in data:
        return jsonify({
            'error': 'Bad Request',
            'message': 'Code book or title or language or year or total book not given'
        }), 400
    b = Book(
        title = data['title'],
        language = data['language'],
        year = data['year'],
        total_book = data['total_book']
    )
    db.session.add(b)
    db.session.commit()
    return {
        'code_book': b.code_book, 'title': b.title, 'language': b.language,
        'year': b.year, 'total_book': b.total_book
    }, 201

@app.route('/books/<id>/', methods=['PUT'])
def update_book(id):
    data = request.get_json()
    if (not 'title' in data) and ('language' not in data) and ('total_book' not in data):
        return {
            'error': 'Bad Request',
            'message': 'Title or language or total book need to be present'
        }, 400
    book = Book.query.filter_by(code_book=id).first_or_404()
    if 'title' in data:
        book.title = data['title']
    if 'language' in data:
        book.language = data['language']
    if 'total_book' in data:
        book.total_book = data['total_book']
    db.session.commit()
    return jsonify({
        'code_book': book.code_book, 'title': book.title, 'language': book.language,
        'year': book.year, 'total_book': book.total_book
    })

@app.route('/books/<id>/', methods=['DELETE'])
def delete_book(id):
    book = Book.query.filter_by(code_book=id).first_or_404()
    db.session.delete(book)
    db.session.commit()
    return {
        'success': 'Data deleted successfully'
    }

@app.route('/rents/')
def get_rents():
    # divide return list from login function into variabel email and password
    email, password = login()
    
    user = Usert.query.filter_by(email=email).first_or_404()
    # return {'password': user.password, 'password2': password}

    # comparing whether the password from filtering by email, and password from header are match
    if bcrypt.check_password_hash(user.password, password):
        return jsonify([
            {
                'rent_id': rent.rent_id, 'rent_from_date': rent.rent_from_date, 'rent_to_date': rent.rent_to_date,
                'actual_return': rent.actual_return_date, 'status': rent.status,
                'borrower': {
                    'name': rent.buser.name,
                    'email': rent.buser.email,
                    'user_id': rent.buser.user_id
                },
                'book': {
                    'title': rent.bbook.title,
                    'language': rent.bbook.language,
                    'year': rent.bbook.year
                }
            } for rent in Rent.query.all()
        ])
    else: return {'error': 'Login Error'}, 401

@app.route('/rents/<id>/')
def get_rent(id):
    # divide return list from login function into variabel email and password
    email, password = login()
    
    user = Usert.query.filter_by(email=email).first_or_404()
    # return {'password': user.password, 'password2': password}

    # comparing whether the password from filtering by email, and password from header are match
    if bcrypt.check_password_hash(user.password, password):
        print(id)
        rent = Rent.query.filter_by(rent_id=id).first_or_404()
        return {
            'rent_id': rent.rent_id, 'rent_from_date': rent.rent_from_date, 'rent_to_date': rent.rent_to_date,
            'actual_return': rent.actual_return_date, 'status': rent.status,
            'borrower': {
                'name': rent.buser.name,
                'email': rent.buser.email,
                'user_id': rent.buser.user_id
            },
            'book': {
                'title': rent.bbook.title,
                'language': rent.bbook.language,
                'year': rent.bbook.year
            }
        }
    else: return {'error': 'Login Error'}, 401

@app.route('/rents/users/<id>')
def get_rents_user(id):
    # divide return list from login function into variabel email and password
    email, password = login()
    
    user = Usert.query.filter_by(email=email).first_or_404()
    # return {'password': user.password, 'password2': password}

    # comparing whether the password from filtering by email, and password from header are match
    if bcrypt.check_password_hash(user.password, password):
        u = Rent.query.filter_by(user_id = id)
        return jsonify([
            {
                'code_book': item.bbook.code_book,
                'title': item.bbook.title,
                'rent_from_date': item.rent_from_date,
                'rent_to_date': item.rent_to_date,
                'actual_return_date': item.actual_return_date
            } for item in Rent.query.filter_by(user_id = id) 
        ])
    else: return {'error': 'Login Error'}, 401

@app.route('/rents/books/<id>')
def get_rents_book(id):
    # divide return list from login function into variabel email and password
    email, password = login()
    
    user = Usert.query.filter_by(email=email).first_or_404()
    # return {'password': user.password, 'password2': password}

    # comparing whether the password from filtering by email, and password from header are match
    if bcrypt.check_password_hash(user.password, password):
        u = Rent.query.filter_by(code_book = id)
        return jsonify([
            {
                'title': item.bbook.title,
                'user_id': item.buser.user_id,
                'name': item.buser.name,
                'rent_from_date': item.rent_from_date,
                'rent_to_date': item.rent_to_date,
                'actual_return_date': item.actual_return_date
            } for item in Rent.query.filter_by(code_book = id) 
        ])
    else: return {'error': 'Login Error'}, 401

@app.route('/rents/', methods=['POST'])
def create_rent():
    # divide return list from login function into variabel email and password
    email, password = login()
    
    user = Usert.query.filter_by(email=email).first_or_404()
    # return {'password': user.password, 'password2': password}

    # comparing whether the password from filtering by email, and password from header are match
    if bcrypt.check_password_hash(user.password, password):
        data = request.get_json()
        if not 'rent_from_date' in data or not 'rent_to_date' in data or not 'user_id' in data or not 'code_book' in data:
            return jsonify({
                'error': 'Bad Request',
                'message': 'Your input not complete'
            }), 400

        selectbook = Book.query.filter_by(code_book = data['code_book']).first_or_404()
        count = Rent.query.filter_by(code_book = data['code_book'], status = False).count()

        if selectbook.total_book <= count:
            return jsonify({
                'error': 'Bad Request',
                'message': 'Book is out of stock'
            })

        r = Rent(
            rent_from_date = data['rent_from_date'],
            rent_to_date = data['rent_to_date'],
            user_id = data['user_id'],
            code_book = data['code_book']
        )
        db.session.add(r)
        db.session.commit()
        return {
            'rent_id': r.rent_id, 'rent_from_date': r.rent_from_date, 'rent_to_date': r.rent_to_date,
            'actual_return': r.actual_return_date, 'status': r.status,
            'borrower': {
                'name': r.buser.name,
                'email': r.buser.email,
                'user_id': r.buser.user_id
            },
            'book': {
                'title': r.bbook.title,
                'language': r.bbook.language,
                'year': r.bbook.year
            }
        }
    else: return {'error': 'Login Error'}, 401

@app.route('/rents/<id>/', methods=['PUT'])
def update_rent(id):
    # divide return list from login function into variabel email and password
    email, password = login()
    
    user = Usert.query.filter_by(email=email).first_or_404()
    # return {'password': user.password, 'password2': password}

    # comparing whether the password from filtering by email, and password from header are match
    if bcrypt.check_password_hash(user.password, password):
        data = request.get_json()
        if ('actual_return_date' not in data) and ('rent_from_date' not in data) and ('rent_to_date' in data) and ('status' not in data):
            return {
                'error': 'Bad Request',
                'message': 'Specify what you need to update'
            }, 400
        r = Rent.query.filter_by(rent_id=id).first_or_404()
        if 'rent_from_date' in data:
            r.rent_from_date = data['rent_from_date']
        if 'rent_to_date' in data:
            r.rent_to_date = data['rent_to_date']
        if 'actual_return_date' in data:
            r.actual_return_date = data['actual_return_date']
        if 'status' in data:
            r.status = data['status']
        if 'user_id' in data:
            r.user_id = data['user_id']
        if 'code_book' in data:
            r.code_book = data['code_book']
        db.session.commit()
        return {
            'rent_id': r.rent_id, 'rent_from_date': r.rent_from_date, 'rent_to_date': r.rent_to_date,
            'actual_return_date': r.actual_return_date, 'status': r.status,
            'borrower': {
                'name': r.buser.name,
                'email': r.buser.email,
                'user_id': r.buser.user_id
            },
            'book': {
                'title': r.bbook.title,
                'language': r.bbook.language,
                'year': r.bbook.year
            }
        }
    else: return {'error': 'Login Error'}, 401
    